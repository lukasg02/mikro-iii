# Mikrocomputertechnik 3

Begleit Project für Mikrocomputertechnik 3 bei TEA19 an der DHBW Ravensburg Campus Friedrichshafen


# Hilfreiche Links

- [Verwendete Tools](doc/tools.adoc)
- [Moodle](https://elearning.dhbw-ravensburg.de/)
- [Markdown Cheat Sheet](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf)
- [Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
